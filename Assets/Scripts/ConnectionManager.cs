using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionManager : MonoBehaviourPunCallbacks
{

    [HideInInspector]
    public bool IsMaster = false;

    private void Awake()
    {
        PhotonNetwork.SendRate = 60;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster() was called by PUN.");
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = false;
        roomOptions.MaxPlayers = 2;
        PhotonNetwork.JoinOrCreateRoom("CrazyCubes", roomOptions, TypedLobby.Default);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("I'm the master!");
        IsMaster = true;
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("I'm in...");
        if(IsMaster)
            PhotonNetwork.Instantiate("GameManager", Vector3.zero, Quaternion.identity);
    }
}
