using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazyPlayerBehaviour : MonoBehaviour
{

    [SerializeField]
    float m_ForceIntensity = 10f;
    [SerializeField]
    float m_LevitationRadiusMagneticEffect = 3.5f;
    [SerializeField]
    float m_LevitationMagneticForceIntensity = 2.5f;
    [SerializeField]
    float m_LevitateForce = 10f;
    [SerializeField]
    float m_MaximumHeightForLevitation = 2.5f;
    [SerializeField]
    float m_KatamariRadiusMagneticEffect = 3.5f;
    [SerializeField]
    float m_KatamariMagneticForceIntensity = 2.5f;

    float m_XDirection = 0f;
    float m_YDirection = 0f;
    bool m_Levitate = false;
    bool m_Katamari = false;

    Rigidbody m_Rb;

    void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        m_YDirection = -Input.GetAxis("Horizontal");
        m_XDirection = Input.GetAxis("Vertical");
        if (Input.GetKey(KeyCode.Space))
        {
            m_Katamari = false;
            m_Levitate = true;
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            m_Levitate = false;
            m_Katamari = true;
        }
        else
        {
            m_Levitate = false;
            m_Katamari = false;
        }
    }

    private void FixedUpdate()
    {
        Vector2 direction = new Vector2(m_XDirection, m_YDirection).normalized;
        float forceIntensityToUse = m_ForceIntensity;

        if (m_Katamari)
        {
            forceIntensityToUse *= 2;
        }

        if (m_Levitate)
        {
            forceIntensityToUse /= 2;
        }


        m_Rb.AddForce(new Vector3(m_XDirection * forceIntensityToUse, 0f, m_YDirection * forceIntensityToUse));

        if (m_Levitate)
        {
            m_Rb.useGravity = false;
            Vector3 positionForLevitation = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
            if (transform.position.y < m_MaximumHeightForLevitation)
                m_Rb.AddForceAtPosition(Vector3.up * m_LevitateForce, positionForLevitation, ForceMode.Acceleration);
            else
                m_Rb.useGravity = true;
            Collider[] colliders = Physics.OverlapSphere(positionForLevitation, m_LevitationRadiusMagneticEffect);

            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();
                if (rb != null && !rb.isKinematic && rb != m_Rb)
                {
                    Vector3 ForceDirection = (hit.transform.position - transform.position).normalized;
                    rb.velocity = (ForceDirection * m_LevitationMagneticForceIntensity);
                }
            }


        }
        else
        {
            m_Rb.useGravity = true;
        }

        if (m_Katamari)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, m_KatamariRadiusMagneticEffect);

            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();
                if (rb != null && !rb.isKinematic && rb != m_Rb)
                {
                    Vector3 ForceDirection = ( transform.position - hit.transform.position).normalized;
                    rb.AddForce(ForceDirection * m_KatamariMagneticForceIntensity, ForceMode.Force);
                }
            }
        }

    }
}
