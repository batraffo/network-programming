using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class CubesSceneNetworkView : MonoBehaviour, IPunObservable
{

    [SerializeField]
    Transform Cubes;
    [SerializeField]
    TextMeshProUGUI text;

    int m_ClientLastExecutedPacketTimestamp = int.MinValue;

    float m_CurrentInterpolationValue = 0;

    [HideInInspector]
    public int TimestampOfTheMostRecentPacketReceivedByTheClient = int.MinValue;

    byte[] CubeStateListToByteArray(List<CubeState> cubes)
    {
        int len = ((80/8)*cubes.Count);

        byte[] arr = new byte[len];

        int indexByte = 0;

        for(int i = 0; i < cubes.Count; i++) {

            arr[indexByte] = (byte) (cubes[i].posX & 255);
            indexByte++;
            arr[indexByte] = (byte) ((cubes[i].posX & 0xFF00)>>8);
            indexByte++;

            arr[indexByte] = (byte)(cubes[i].posY);
            indexByte++;

            arr[indexByte] = (byte)(cubes[i].posZ & 255);
            indexByte++;
            arr[indexByte] = (byte)((cubes[i].posZ & 0xFF00) >> 8);
            indexByte++;

            arr[indexByte] = (byte)(cubes[i].rotX);
            indexByte++;
            arr[indexByte] = (byte)(cubes[i].rotY);
            indexByte++;
            arr[indexByte] = (byte)(cubes[i].rotZ);
            indexByte++;

            arr[indexByte] = (byte)(cubes[i].index & 255);
            indexByte++;
            arr[indexByte] = (byte)((cubes[i].index & 0xFF00) >> 8);
            indexByte++;

        }

        return arr;
    }

    List<CubeState> ByteArrayToCubeStateList(byte[] bytearray)
    {
        List<CubeState> list = new List<CubeState>();

        int indexByte = 0;

        while(indexByte < bytearray.Length)
        {
            CubeState cb = new CubeState();
            
            cb.posX =(short)((bytearray[indexByte]) + (bytearray[++indexByte]<<8));
            indexByte++;

            cb.posY = bytearray[indexByte];
            indexByte++;

            cb.posZ = (short)((bytearray[indexByte]) + (bytearray[++indexByte] << 8));
            indexByte++;

            cb.rotX = (sbyte) bytearray[indexByte];
            indexByte++;

            cb.rotY = (sbyte)bytearray[indexByte];
            indexByte++;

            cb.rotZ = (sbyte)bytearray[indexByte];
            indexByte++;

            cb.index = (short)((bytearray[indexByte]) + (bytearray[++indexByte] << 8));
            indexByte++;

            list.Add(cb);
        }


        return list;
    }

    public bool AreTheSameCubeState(CubeState cs1, CubeState cs2)
    {
        if(cs1.posY == cs2.posY && cs1.posX == cs2.posX && cs1.posZ == cs2.posZ &&
            cs1.rotX == cs2.rotX && cs1.rotY == cs2.rotY && cs1.rotZ == cs2.rotZ)
        {
            return true;
        }

        return false;
    }

    public struct CubeState
    {
        //Not sended, only used by the Master
        public int lastTimeChanged;
        
        public short posX;
        public byte posY;
        public short posZ;

        public sbyte rotX;
        public sbyte rotY;
        public sbyte rotZ;

        public short index;

    }

    struct Packet
    {
        public List<CubeState> m_CubesStates;
        public int m_ServerTimestamp;

        public Packet(List<CubeState> CubesStates, int ServerTimestamp)
        {
            m_CubesStates = CubesStates;
            m_ServerTimestamp = ServerTimestamp;
        }
    }

    
    CubeState[] CubesStatesPacket = new CubeState[901];
    List<Packet> ReaderPacketsQueue = new List<Packet>();
    

    private void OnEnable()
    {
        PhotonNetwork.SendRate = 60;
        if (PhotonNetwork.IsMasterClient)
        {
            if (Cubes != null)
            {
                foreach (Transform t in Cubes)
                {
                    Rigidbody cubeRb = t.gameObject.GetComponent<Rigidbody>();
                    cubeRb.isKinematic = false;
                }
            }
        }
        else
        {
            GameObject gameObject = PhotonNetwork.Instantiate("DeltaCompressor", Vector3.zero, Quaternion.identity);
            gameObject.GetComponent<DeltaCompressorNetworkView>().CubesSceneNetworkView = this.GetComponent<CubesSceneNetworkView>();
        }
    }

    private void Update()
    {
        reExecuteUpdate:
        if (ReaderPacketsQueue.Count > 1)
        {
            if (((ReaderPacketsQueue[1].m_ServerTimestamp - ReaderPacketsQueue[0].m_ServerTimestamp) > Time.smoothDeltaTime * 1000)  )
            {
                if (m_CurrentInterpolationValue <= 1f)
                {
                    m_CurrentInterpolationValue += 1f /

                        ((float)((ReaderPacketsQueue[1].m_ServerTimestamp - ReaderPacketsQueue[0].m_ServerTimestamp)) /
                        (Time.smoothDeltaTime * 1000));

                    InterpolatePackets(ReaderPacketsQueue[0].m_CubesStates, ReaderPacketsQueue[1].m_CubesStates, m_CurrentInterpolationValue);
                }
                else
                {
                    m_ClientLastExecutedPacketTimestamp = ReaderPacketsQueue[0].m_ServerTimestamp;
                    ReaderPacketsQueue.RemoveAt(0);
                    m_CurrentInterpolationValue = 0f;
                    goto reExecuteUpdate;
                }
            }
            else
            {
                ConsumePacket((ReaderPacketsQueue[0].m_CubesStates));
                m_ClientLastExecutedPacketTimestamp = ReaderPacketsQueue[0].m_ServerTimestamp;
                ReaderPacketsQueue.RemoveAt(0);
                m_CurrentInterpolationValue = 0f;
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //I'm the master
        if (stream.IsWriting)
        {
            List<CubeState> cubeStatesToSend = new List<CubeState>();
            int i = 0;
            foreach(Transform t in Cubes)
            {

                CubeState cubeState = new CubeState();

                cubeState.posX = (short) Math.Round(t.position.x / (1f / 218f));
                cubeState.posZ = (short) Math.Round(t.position.z / (1f / 218f));
                cubeState.posY = (byte) Math.Round(t.position.y / (0.04f));

                cubeState.rotX = (sbyte)Math.Round(t.rotation.x / (1f / 127f));
                cubeState.rotY = (sbyte)Math.Round(t.rotation.y / (1f / 127f));
                cubeState.rotZ = (sbyte)Math.Round(t.rotation.z / (1f / 127f));

                if(t.rotation.w < 0)
                {
                    cubeState.rotX = (sbyte) -cubeState.rotX;
                    cubeState.rotY = (sbyte) -cubeState.rotY;
                    cubeState.rotZ = (sbyte) -cubeState.rotZ;
                }

                cubeState.index = (short) i;

                if(!AreTheSameCubeState(CubesStatesPacket[i], cubeState))
                {
                    CubesStatesPacket[i] = cubeState;
                    cubeState.lastTimeChanged = info.SentServerTimestamp;
                    cubeStatesToSend.Add(cubeState);
                }
                i++;
            }

            byte[] bytesToSend = CubeStateListToByteArray(cubeStatesToSend);
            text.text = "You are sending " + bytesToSend.Count() + " bytes";
            stream.SendNext(bytesToSend);

        }

        //I'm the client
        if (stream.IsReading)
        {
            //Debug.Log(info.SentServerTimestamp);


            if (info.SentServerTimestamp < m_ClientLastExecutedPacketTimestamp)
            {
                stream.ReceiveNext();
            }

            else
            {

                if (ReaderPacketsQueue.Count == 0)
                {
                    byte[] cubeStateArrayToConvert = (byte[])stream.ReceiveNext();
                    List<CubeState> cubeStatesList = ByteArrayToCubeStateList(cubeStateArrayToConvert);

                    ReaderPacketsQueue.Add(new Packet(cubeStatesList, info.SentServerTimestamp));
                    TimestampOfTheMostRecentPacketReceivedByTheClient = info.SentServerTimestamp;
                }
                else
                {
                    int index = 0;

                    while (index < ReaderPacketsQueue.Count)
                    {

                        if (info.SentServerTimestamp > ReaderPacketsQueue[index].m_ServerTimestamp)
                            index++;
                        else if (info.SentServerTimestamp == ReaderPacketsQueue[index].m_ServerTimestamp)
                        {
                            stream.ReceiveNext();
                            break;
                        }
                        else if (info.SentServerTimestamp < ReaderPacketsQueue[index].m_ServerTimestamp)
                        {
                            if (index == 0)
                            {
                                stream.ReceiveNext();
                                break;
                            }

                            byte[] cubeStateArrayToConvert = (byte[])stream.ReceiveNext();
                            List<CubeState> cubeStatesList = ByteArrayToCubeStateList(cubeStateArrayToConvert);

                            ReaderPacketsQueue[index] = new Packet(cubeStatesList, info.SentServerTimestamp);
                            if(ReaderPacketsQueue.Count > 3)
                                ReaderPacketsQueue.RemoveAt(0);
                            break;

                        }

                    }

                    if (info.SentServerTimestamp > ReaderPacketsQueue[ReaderPacketsQueue.Count - 1].m_ServerTimestamp)
                    {
                        byte[] cubeStateArrayToConvert = (byte[])stream.ReceiveNext();
                        List<CubeState> cubeStatesList = ByteArrayToCubeStateList(cubeStateArrayToConvert);

                        ReaderPacketsQueue.Add(new Packet(cubeStatesList, info.SentServerTimestamp));
                        TimestampOfTheMostRecentPacketReceivedByTheClient = info.SentServerTimestamp;
                        if (ReaderPacketsQueue.Count > 3)
                        {
                            ReaderPacketsQueue.RemoveAt(0);
                            m_CurrentInterpolationValue = 0;
                        }

                    }

                }

            }

        }
    }

    private void InterpolatePackets(List<CubeState> m_CubesStates1, List<CubeState> m_CubesStates2, float m_CurrentInterpolationValue)
    {
        m_CubesStates1.Sort((cb1, cb2) => cb1.index.CompareTo(cb2.index));
        m_CubesStates2.Sort((cb1, cb2) => cb1.index.CompareTo(cb2.index));

        int listIndex1 = 0;
        int listIndex2 = 0;

        for (int i = 0; i < Cubes.childCount; i++)
        {

            if (listIndex1 < m_CubesStates1.Count && m_CubesStates1[listIndex1].index == i)
            {
                Transform cubeTransform = Cubes.GetChild(m_CubesStates1[listIndex1].index);

                CubeState cubeState1 = m_CubesStates1[listIndex1];

                bool foundCorrespondence = false;

                while (listIndex2 < (m_CubesStates2.Count - 1) &&  m_CubesStates2[listIndex2].index < m_CubesStates1[listIndex1].index) {
                    listIndex2++;
                }

                if (m_CubesStates2.Count > 0 && m_CubesStates2[listIndex2].index == m_CubesStates1[listIndex1].index)
                {
                    foundCorrespondence = true;
                }


                if (!foundCorrespondence)
                {

                    cubeTransform.position = new Vector3(
                    cubeState1.posX * (1f / 218f),
                    cubeState1.posY * (0.04f),
                        cubeState1.posZ * (1f / 218f)
                    );

                    float rotX = cubeState1.rotX * (1f / 127f);
                    float rotY = cubeState1.rotY * (1f / 127f);
                    float rotZ = cubeState1.rotZ * (1f / 127f);
                    float w = (float)Math.Sqrt(1 - (rotX * rotX + rotY * rotY + rotZ * rotZ));

                    cubeTransform.rotation = new Quaternion(
                        rotX,
                        rotY,
                        rotZ,
                        w
                        );

                    cubeTransform.GetComponent<ChangeColorOnVelocityChange>()?.MakeCubeRed();

                    listIndex1++;
                }
                else
                {

                    CubeState cubeState2 = m_CubesStates2[listIndex2];

                    Vector3 firstVector = new Vector3(
                    cubeState1.posX * (1f / 218f),
                    cubeState1.posY * (0.04f),
                        cubeState1.posZ * (1f / 218f)
                    );

                    Vector3 secondVector = new Vector3(
                    cubeState2.posX * (1f / 218f),
                    cubeState2.posY * (0.04f),
                        cubeState2.posZ * (1f / 218f)
                    );

                    cubeTransform.position = Vector3.Lerp(firstVector, secondVector, m_CurrentInterpolationValue);

                    float rotX1 = cubeState1.rotX * (1f / 127f);
                    float rotY1 = cubeState1.rotY * (1f / 127f);
                    float rotZ1 = cubeState1.rotZ * (1f / 127f);
                    float w1 = (float)Math.Sqrt(1 - (rotX1 * rotX1 + rotY1 * rotY1 + rotZ1 * rotZ1));

                    float rotX2 = cubeState2.rotX * (1f / 127f);
                    float rotY2 = cubeState2.rotY * (1f / 127f);
                    float rotZ2 = cubeState2.rotZ * (1f / 127f);
                    float w2 = (float)Math.Sqrt(1 - (rotX2 * rotX2 + rotY2 * rotY2 + rotZ2 * rotZ2));

                    Quaternion firstRotation = new Quaternion(
                        rotX1,
                        rotY1,
                        rotZ1,
                        w1
                        );

                    Quaternion secondRotation = new Quaternion(
                        rotX2,
                        rotY2,
                        rotZ2,
                        w2
                        );

                    cubeTransform.rotation = Quaternion.Slerp(firstRotation, secondRotation, m_CurrentInterpolationValue);

                    cubeTransform.GetComponent<ChangeColorOnVelocityChange>()?.MakeCubeRed();

                    listIndex1++;
                }
            }
            else
            {
                Transform cubeTransform = Cubes.GetChild(i);

                cubeTransform.GetComponent<ChangeColorOnVelocityChange>()?.MakeCubeOriginalColor();
            }




        }
    }


    private void ConsumePacket(List<CubeState> states)
    {
        states.Sort((cb1, cb2) => cb1.index.CompareTo(cb2.index));

        int listIndex = 0;

        for (int i = 0; i < Cubes.childCount; i++)
        {



            if (listIndex < states.Count && states[listIndex].index == i)
            {
                CubeState cubeState = states[listIndex];
                Transform cubeTransform = Cubes.GetChild(cubeState.index);
                cubeTransform.position = new Vector3(
                cubeState.posX * (1f / 218f),
                cubeState.posY * (0.04f),
                    cubeState.posZ * (1f / 218f)
                );

                float rotX = cubeState.rotX * (1f / 127f);
                float rotY = cubeState.rotY * (1f / 127f);
                float rotZ = cubeState.rotZ * (1f / 127f);
                float w = (float)Math.Sqrt(1 - (rotX * rotX + rotY * rotY + rotZ * rotZ));

                cubeTransform.rotation = new Quaternion(
                    rotX,
                    rotY,
                    rotZ,
                    w
                    );

                cubeTransform.GetComponent<ChangeColorOnVelocityChange>()?.MakeCubeRed();

                listIndex++;
            }
            else
            {
                Transform cubeTransform = Cubes.GetChild(i);

                cubeTransform.GetComponent<ChangeColorOnVelocityChange>()?.MakeCubeOriginalColor();
            }

            
        }
    }
}
