using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DeltaCompressorNetworkView : MonoBehaviour, IPunObservable
{
    [HideInInspector]
    public CubesSceneNetworkView CubesSceneNetworkView;

    private void Awake()
    {
        PhotonNetwork.SendRate = 60;
    }

    private void OnEnable()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            var GameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            foreach(var gameObject in GameObjects)
            {
                if (gameObject.name.Equals("GameManager(Clone)"))
                {
                    CubesSceneNetworkView = gameObject.GetComponent<CubesSceneNetworkView>();
                }
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //I'm the client
        if (stream.IsWriting)
        {
            stream.SendNext(CubesSceneNetworkView?.TimestampOfTheMostRecentPacketReceivedByTheClient);
        }

        //I'm the master
        if (stream.IsReading)
        {
            if (CubesSceneNetworkView != null) {
                int newValue = (int) stream.ReceiveNext();

                if (newValue > CubesSceneNetworkView.TimestampOfTheMostRecentPacketReceivedByTheClient)
                    CubesSceneNetworkView.TimestampOfTheMostRecentPacketReceivedByTheClient = newValue;
            }
            else
                stream.ReceiveNext();
        }

    }
}
