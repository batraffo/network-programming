using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ChangeColorOnVelocityChange : MonoBehaviour
{
    public int Index;
    
    Rigidbody m_Rb;
    Renderer m_Renderer;
    Color m_InitialColor;
    bool m_FirstExecution = true;



    private void Awake()
    {
        string number = Regex.Match(name, @"\d+").Value;
        Index = int.Parse(number);
    }

    void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
        m_Renderer = GetComponent<Renderer>();
        if(m_Renderer != null)
        {
            m_InitialColor = m_Renderer.material.color;
        }
    }

    void FixedUpdate()
    {
        
        if (!m_Rb.isKinematic)
        {
            //MOVIN' CUBE
            MakeCubeRed();

            //STOPPIN' CUBE
            MakeCubeOriginalColor();
        }
    }

    public void MakeCubeOriginalColor()
    {
        if (m_Rb != null && (m_Rb.velocity == Vector3.zero || m_Rb.isKinematic))
        {
            if (m_Renderer != null && m_Renderer.material.color == Color.red)
            {
                StartCoroutine(ReturnToInitialColor());
            }
        }
    }

    public void MakeCubeRed()
    {
        if (m_Rb != null && (m_Rb.velocity != Vector3.zero || m_Rb.isKinematic))
        {
            if (m_Renderer != null && m_Renderer.material.color == m_InitialColor)
            {
                StartCoroutine(BecomeRed());
            }
        }
    }

    IEnumerator BecomeRed()
    {
        if (!m_FirstExecution)
        {
            for (int i = 0; i < 15; i++)
            {
                m_Renderer.material.color = Color.Lerp(m_InitialColor, Color.red, (i + 1f) / 15f);
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            yield return new WaitForSeconds(0.25f);
            m_FirstExecution = false;
        }
    }

    IEnumerator ReturnToInitialColor()
    {
        for (int i = 0; i < 15; i++)
        {
            m_Renderer.material.color = Color.Lerp(Color.red, m_InitialColor, (i + 1f) / 15f);
            yield return new WaitForSeconds(0.1f);
        }
    }

}
