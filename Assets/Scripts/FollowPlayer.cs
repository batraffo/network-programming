using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    [SerializeField]
    Transform PlayerTransform;
    [SerializeField]
    Vector3 CameraDistance = new Vector3(-2.5f, 2f, 0);

    private void LateUpdate()
    {
        if(PlayerTransform!= null)
        {
            this.transform.position = new Vector3(PlayerTransform.position.x + CameraDistance.x,
                PlayerTransform.position.y + CameraDistance.y,
                PlayerTransform.position.z + CameraDistance.z);
        }
    }
}
