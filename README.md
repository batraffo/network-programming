# Network Programming Project

This project was developed for the **Network Programming** exam. We have to synchronize a physics scene with **901 cubes with rigid bodies** between two network clients.

A cube is controlled by **a player, who is also the *master client***, and it's moved using forces on it. The player's cube can also levitate and push away other
cubes or rotate while attracting the other cubes to itself.
The scene in which the player interacts with all the cubes should be *almost the same* on the other client.

## Technique and technology

I created the scene in **Unity** and I have used **Photon Unity Networking** for exchanging UDP packets.

I used the **Snapshot Interpolation** technique for syncing the scene.

I decided to send **60 packets/sec** from the *master client* to improve the client simulation, since I used **linear interpolation**, and to save bandwidth by not sending the velocity of the cubes used by more complex interpolation. The packets contain the rotations (only x, y and z: I calculate w from these three values), the positions (both **bounded and quantized**, as you can read in the next section of the README) and the indexes of the cubes that were moved by the *master client*.
Creating a sort of **reliability layer** over UDP, I send the packets only relative to the last changes in position and rotation of the cubes that have occurred since the last received packet of the *client*, using a technique called **Lambda Compression**.

From the *client* side I used a queue where I saved the last three packets so the *client* could interpolate between two packets over time.

## Logical Packet structure


        short posX; //16 bits  precision of 0.00458, represents values between   [-150, 150]
        byte posY;  //8 bits   precision of 0.04,    represents values between   [0,10]
        short posZ; //16 bits  precision of 0.00458, represents values between   [-150, 150]

        byte rotX; //8 bits  precision of 0.00787,  represents values between   [-1,1]
        byte rotY; //8 bits  precision of 0.00787,  represents values between   [-1,1]
        byte rotZ; //8 bits  precision of 0.00787,  represents values between   [-1,1]

        short index; //16 bits 0...901


Total packet size = 80 bits * **the number of cubes that have changed since the last packet for which I received an ACK**
## Results
The simulation on the *client* side is good and the *master client* sends only 50-150 KBit/sec. 

## Possible improvements
I am not completely satisfied with the simulated rotation, even with the interpolation done. One improvement that I had in mind was to use 2 of the unused bits from the **index field** for the **smallest three** technique. With this technique, I can dynamically decide which data *NOT* to send for the rotation by selecting the highest component between x, y, z and w. These two bits just notify the *client* which of these components it should calculate.

Another way to improve the rotation is by using the other 3 unused bits from the **index field** to represent the sign of the three rotation components that I send, so that I can improve the precision of the sent data with one additional bit in the byte.
